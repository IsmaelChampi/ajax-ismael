//tipos de promesa
console.log('hola mundo!');
const noCambia = "Leonidas";

let cambia = "@LeonidasEsteban"

function cambiarNombre(nuevoNombre) {
  cambia = nuevoNombre
}
/////////////////////////////////////////////////////////////////////
// ejemplo de promesa

const getUserAll = new Promise(function(todobien, todomal){
     setTimeout(function(){
  	 //luego de 3 seg
  	 todobien('se acabo el tiempo 2');
     }, 5000)
})

const getUser = new Promise(function(todobien, todomal){
     setTimeout(function(){
  	 //luego de 3 seg
  	 todobien('se acabo el tiempo 1');
     }, 3000)
})

// getUser
//     .then(function() {
//        console.log('todo esta bien en la vida')
//     })
//     .catch(function(msg) {
//     	console.log(msg)
//     })

//race para una carrera de promesas literal
// y solo entra al then que acaba primero
Promise.all([
	getUser,
	getUserAll,
])
//metodo si todo a ido bien
.then(function(message){
	console.log(message);
})
//metodo cuando se rechaza o falla
.catch(function(message){
	console.log(message);
})
/////////////////////////////////////////////////////////////////////
//Ajax - JQuery - Javascript
$.ajax('https://randomuser.me/api/sdfdsfdsfs', {
	method: 'GET',
	success: function(data) {
	   console.log(data)
	},
	error: function(error) {
		console.log(error)
	}
})
//Pedir datos del nuevo javascript
//funcion adecuada
fetch('https://randomuser.me/api/jhagdjhgsajh')
	.then(function (response) {
		// console.log(response)
	 return response.json()

	})
	.then(function (user) {
		console.log('user' , user.results[0].name.first)
	})
	.catch(function (user) {
		console.log('fallo ismael')
	})
//////////////////////////////////////////////////////////////////
//FUNCIONES ASINCRONAS
//dentro 
async function getData(url) {

	const response = await fetch(url);
	const data = await response.json()
	return data;
}









